import { defineConfig } from "vite"
import { svelte } from "@sveltejs/vite-plugin-svelte"
import { visualizer } from "rollup-plugin-visualizer"

export default defineConfig({
  base: "./",
  plugins: [
    svelte(),
    visualizer({
      emitFile: true,
      filename: "stats.html",
    }),
  ],
})
