import "@fontsource/source-sans-pro/latin-400.css"
import "@fontsource/source-sans-pro/latin-700.css"
import "material-icons/iconfont/material-icons.css"
import "svelte-material-ui/bare.css"
import "./font-replacement.css"
import "./color-replacement.css"
import "./global.css"
