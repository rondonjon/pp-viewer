import type { ResolvedNode } from "load-portfolio-performance-xml"
import type { UUID } from "../types/UUID"

export type Security = {
  name?: string
  tickerSymbol?: string
  uuid: string
}

export type Securities = Record<UUID, Security>

export const getSecurities = (doc: ResolvedNode): Securities => {
  const security = doc?.client?.[0]?.securities?.[0]?.security || []
  const result: Securities = {}

  for (const s of security) {
    const uuid = s.uuid?.[0]?.["#text"] || ""
    result[uuid] = {
      uuid,
      name: s.name?.[0]?.["#text"],
      tickerSymbol: s.tickerSymbol?.[0]?.["#text"],
    }
  }

  return result
}
