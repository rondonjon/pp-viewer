import type { ResolvedNode } from "load-portfolio-performance-xml"

export const getVersion = (doc: ResolvedNode): string | undefined => doc.client?.[0]?.version?.[0]?.["#text"]
