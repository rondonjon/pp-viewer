import type { ResolvedNode } from "load-portfolio-performance-xml"
import type { UUID } from "../types/UUID"

export type Holding = {
  quantity: number
  avgPrice: number
}

export type Holdings = Record<UUID, Holding>

export const getHoldings = (doc: ResolvedNode): Holdings => {
  const portfolio = doc.client?.[0]?.portfolios?.[0]?.portfolio || []
  const holdings: Holdings = {}

  for (const p of portfolio) {
    const portfolioTransaction = p.transactions?.[0]?.["portfolio-transaction"] || []
    for (const pt of portfolioTransaction) {
      const uuid = pt.security[0].uuid[0]["#text"]
      const amount = Number(pt.amount[0]["#text"]) / 100
      const quantity = Number(pt.shares[0]["#text"]) / 100000000
      const avgPrice = amount / quantity
      const type = pt.type[0]["#text"]

      const prevQuantity = holdings[uuid]?.quantity || 0
      const prevAvgPrice = holdings[uuid]?.avgPrice || 0

      let newQuantity: number
      let newAvgPrice: number

      if (type === "BUY") {
        newQuantity = prevQuantity + quantity
        newAvgPrice = (prevQuantity * prevAvgPrice + quantity * avgPrice) / newQuantity
      } else if (type === "SELL") {
        newQuantity = prevQuantity - quantity
        newAvgPrice = prevAvgPrice
      } else {
        continue
      }

      holdings[uuid] = {
        quantity: newQuantity,
        avgPrice: newAvgPrice,
      }
    }
  }

  return holdings
}
