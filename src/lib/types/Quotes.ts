import type { Quote } from "../yahoo/quote"
import type { TickerSymbol } from "./TickerSymbol"

export type Quotes = Record<TickerSymbol, Quote>
