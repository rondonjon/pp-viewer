import crossFetch from "cross-fetch"

export type Module = "financialData" | "defaultKeyStatistics" | "assetProfile"

export const quoteSummary = async (symbol: string, modules: Module[]) => {
  const url = new URL("https://query1.finance.yahoo.com/v10/finance/quoteSummary/")
  url.searchParams.set("symbol", symbol)
  url.searchParams.set("modules", modules.join(","))

  const response = await crossFetch(url)
  const data = await response.json()

  if (!data.quoteSummary) {
    throw new Error(`Missing "quoteSummary" in response`)
  } else if (data.quoteSummary.error) {
    throw new Error(data.quoteSummary.error.description)
  } else if (!Array.isArray(data.quoteSummary.result)) {
    throw new Error(`Missing "quoteSummary.result" array in response`)
  }

  return data.quoteSummary.result
}
