import crossFetch from "cross-fetch"

export type Quote = {
  language: string
  region: string
  quoteType: string
  typeDisp: string
  quoteSourceName: string
  triggerable: boolean
  customPriceAlertConfidence: string
  exchangeTimezoneName: string
  exchangeTimezoneShortName: string
  gmtOffSetMilliseconds: number
  market: string
  esgPopulated: boolean
  exchange: string
  marketState: string
  tradeable: boolean
  firstTradeDateMilliseconds: number
  priceHint: number
  regularMarketTime: number
  regularMarketPrice: number
  sourceInterval: number
  exchangeDataDelayedBy: number
  fullExchangeName: string
  symbol: string
}

export const quote = async (symbols: string[]) => {
  const url = new URL("https://query1.finance.yahoo.com/v7/finance/quote")
  url.searchParams.set("symbols", symbols.join(","))
  url.searchParams.set("fields", "regularMarketPrice")

  const response = await crossFetch(url)
  const data = await response.json()

  if (!data.quoteResponse) {
    throw new Error(`Missing "quoteResponse" in response`)
  } else if (data.quoteResponse.error) {
    throw new Error(data.quoteResponse.error.description)
  } else if (!Array.isArray(data.quoteResponse.result)) {
    throw new Error(`Missing "quoteResponse.result" array in response`)
  }

  return data.quoteResponse.result as Quote[]
}
