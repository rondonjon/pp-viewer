export const isValidTickerSymbol = (candidate: string) =>
  candidate.match(/^\^[A-Z0-9]+$/) || // index
  candidate.match(/^[A-Z0-9]+\.[A-Z0-9]+$/) // security with exchange id
