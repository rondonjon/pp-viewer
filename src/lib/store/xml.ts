import { loadPortfolioPerformanceXml } from "load-portfolio-performance-xml"
import { writable } from "svelte/store"
import { getHoldings } from "../pp/getHoldings"
import { getSecurities } from "../pp/getSecurities"
import { document } from "./document"
import { holdings } from "./holdings"
import { securities } from "./securities"

export const xml = writable<string | Buffer | undefined>(undefined)

xml.subscribe((data) => {
  document.set(undefined)
  holdings.set({})
  securities.set({})

  if (data) {
    setTimeout(() => {
      try {
        const newDocument = loadPortfolioPerformanceXml(data)
        const newHoldings = getHoldings(newDocument)
        const newSecurities = getSecurities(newDocument)

        document.set(newDocument)
        holdings.set(newHoldings)
        securities.set(newSecurities)
      } catch (err) {
        alert(err)
      }
    }, 0)
  }
})
