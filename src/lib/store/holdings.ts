import { writable } from "svelte/store"
import type { Holdings } from "../pp/getHoldings"

export const holdings = writable<Holdings>({})
