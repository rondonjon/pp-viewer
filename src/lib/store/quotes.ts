import { writable } from "svelte/store"
import type { Quotes } from "../types/Quotes"

export const quotes = writable<Quotes>({})
