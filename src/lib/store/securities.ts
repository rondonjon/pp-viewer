import { writable } from "svelte/store"
import { type Securities } from "../pp/getSecurities"

export const securities = writable<Securities>({})
