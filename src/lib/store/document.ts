import { type ResolvedNode } from "load-portfolio-performance-xml"
import { writable } from "svelte/store"

export const document = writable<ResolvedNode | undefined>(undefined)
