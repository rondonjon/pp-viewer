import "./styles"
import App from "./lib/components/App/index.svelte"
import { xml } from "./lib/store/xml"

const app = new App({
  target: document.getElementById("app"),
})

if (process.env.NODE_ENV === "development") {
  setTimeout(async () => {
    try {
      const response = await fetch(
        "https://raw.githubusercontent.com/buchen/portfolio/master/name.abuchen.portfolio.ui/src/name/abuchen/portfolio/ui/parts/kommer.xml"
      )
      const text = await response.text()
      console.log(text)
      xml.set(text.replace(/<tickerSymbol>([A-Z0-9]{3,})/g, `<tickerSymbol>$1.DE`))
    } catch (err) {
      alert(err)
    }
  }, 0)
}

export default app
